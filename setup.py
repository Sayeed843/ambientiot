"""
Thesis title : Autonomous service creation framework for pluggable IoT Sensors and Actuators
Contributor : Abu Sayeed Bin Mozahid (151-35-843) & MD. Ariful Islam (151-35-937)
Supervisor : K. M. Imtiaz-Ud-Din (Assistant Professor)
Department of Software Engineering
Daffodil International University
Summer 2019
"""

import os
import time

from ArduinoIDE.arduino import NodeMCUInstallation
from ArduinoIDE.file_configure import NodeMCUFile


devices = {
    '0':'actuator',
    '1':'soil_moisture_sensor',
    '2':'ultrasonic_sensor_hc_sr04',
    '3':'ir_sensor',
    '4':'analog_sensor',
    '5':'digital_sensor',
}

commands = {
    "arduino-cli": "curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh",
    "move": "sudo mv bin/arduino-cli /usr/bin",
    "init": "arduino-cli config init",
    "update": "arduino-cli core update-index",
    "install": "arduino-cli core install esp8266:esp8266@2.7.4",
    "json": "arduino-cli lib install ArduinoJson@5.13.5",
    "serial-port": "arduino-cli board list",
    "port-permission": "sudo chmod 777 {}",
    "board-check": "arduino-cli board listall | grep -i 'nodemcu'",
    "compile": "arduino-cli compile --fqbn esp8266:esp8266:nodemcuv2 {f}",
    "upload": "arduino-cli upload --port {p} --fqbn esp8266:esp8266:nodemcuv2 {f}",
}


def clear_console():
    os.system("clear")

def sleep(t):
    time.sleep(t)




if __name__ == '__main__':
    node_file = NodeMCUFile()
    nodemcu = NodeMCUInstallation("arduino-cli board listall | grep -i 'nodemcu'")
    clear_console()

    print("Please choice your device name-")
    print("0: Actuator")
    print("1: Soil Moisture Sensor")
    print("2: Ultrasonic Sensor HC-SR04")
    print("3: IR Sensor")
    print("4: Analog Sensor")
    print("5: Digital Sensor")
    print("\n")


    d = input("Enter your choice: ")
    owner = input("Sensor Owner Name:")
    device = devices[d]
    ssid = input("Enter your Wifi Network Name: ")
    password  = input("Enter Wifi Password: ")
    longitude = input("Enter Device Longitude: ")
    latitude = input("Enter Device Latitude: ")
    # api = input("Enter your system api: ")

    node_file.delete_file(device)
    folder_path = node_file.create('ArduinoIDE/file/', owner=owner, ssid=ssid, password=password,
            device=device, longitude=longitude, latitude=latitude)#, api= api)

    # Arduino-cli download
    print(commands['arduino-cli'])
    nodemcu.installation(commands['arduino-cli'])
    sleep(1.0)

    # Arduino-cli file move
    print(commands['move'])
    nodemcu.move(commands['move'])
    sleep(1.0)

    # Initialize the arduino-cli
    print(commands['init'])
    nodemcu.initial_arduino_cli(commands['init'])
    sleep(1.0)
    clear_console()

    # Add json file to yaml file
    nodemcu.modify_yaml()
    sleep(1.0)

    # Update Arduino-cli board index
    print(commands['update'])
    nodemcu.update_board(commands['update'])
    sleep(1.0)

    # Install esp8266 library
    print(commands['install'])
    nodemcu.installation(commands['install'])
    sleep(1.0)

    # Install ArduinoJson library
    print(commands['json'])
    nodemcu.arduino_json(commands['json'])
    sleep(1.0)

    # Check the serial port
    print(commands['serial-port'])
    port = nodemcu.serial_port(commands['serial-port'])
    sleep(1.0)

    # Serial port permission
    print(commands['port-permission'].format(port))
    nodemcu.port_permission(port, commands['port-permission'])
    sleep(1.0)


    # Update Arduino-cli board index
    print(commands['update'])
    nodemcu.update_board(commands['update'])
    sleep(1.0)

    # Check the nodemcu board
    print(commands['board-check'])
    nodemcu.check(commands['board-check'])
    sleep(1.0)

    # Compile the .ino file
    nodemcu.compile(folder=folder_path, cmd=commands['compile'])
    sleep(1.0)

    # Upload the .ino file
    nodemcu.upload(folder=folder_path, port=commands['serial-port'], cmd=commands['upload'])
    sleep(1.0)
