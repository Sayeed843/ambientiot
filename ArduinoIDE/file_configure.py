"""
Thesis title : Autonomous service creation framework for pluggable IoT Sensors and Actuators
Contributor : Abu Sayeed Bin Mozahid (151-35-843) & MD. Ariful Islam (151-35-937)
Supervisor : K. M. Imtiaz-Ud-Din (Assistant Professor)
Department of Software Engineering
Daffodil International University
Summer 2019
"""

import os
import shutil

devices = {
    '0':'actuator',
    '1':'soil_moisture_sensor',
    '2':'ultrasonic_sensor_hc_sr04',
    '3':'ir_sensor',
    '4':'analog_sensor',
    '5':'digital_sensor',
}



class ArduinoFile():
    def __init__(self):
        pass


    # Command- arduino-cli sketch new filename
    def create(self, cmd, filename):
        cmd = cmd+" "+ filename
        output = self.term.task(cmd)
        return filename




class NodeMCUFile(ArduinoFile):

    def __init__(self):
        path = os.getcwd() +"/sketchbook"
        if os.path.exists(path):
            print("Exist")
            shutil.rmtree(path)


    def create_dir(self,device_name):
        folder_path = os.getcwd() +"/sketchbook/"+device_name

        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        return folder_path

    def default_file(self, devicename, path='file/'):
        file_path = path+devicename+'_default.ino'
        with open(file_path, 'r') as f:
            f = f.readlines()
        return f

    def create(self, path, **kwargs):
        file = self.default_file(kwargs['device'], path)
        self.print_data(**kwargs)

        if file is not None:
            return self.file_modification(file, **kwargs)
        else:
            print("Unable to find the default file.")
        return None

    def print_data(self, **kwargs):
        print("\n")
        print("\n")
        print("*"*32)
        print(f"{'*'*3}- Ambient Intlligent Lab -{'*'*3}")
        print("*"*32)

        print(f"Ower Name:  {kwargs['owner']} \t|| Device Name:  {kwargs['device']}")
        print(f"Wifi Name:  {kwargs['ssid']} \t|| Wifi Password:  ****")
        print(f"Logitude:  {kwargs['longitude']} \t|| Latitude:  {kwargs['latitude']}")
        print("\n")
        print("\n")


    def file_modification(self, file, **kwargs):

        folder_path = self.create_dir(kwargs['device'])

        for line in file:
            line = line.replace("owner_name", kwargs['owner'])
            line = line.replace("wifi_name", kwargs['ssid'])
            line = line.replace("wifi_password", kwargs['password'])
            line = line.replace("device_name", kwargs['device'])
            line = line.replace("sensor_long", kwargs['longitude'])
            line = line.replace("actuator_long", kwargs['longitude'])
            line = line.replace("sensor_lati", kwargs['latitude'])
            line = line.replace("actuator_lati",kwargs['latitude'])
            # line = line.replace("system_api", kwargs['api'])

            with open(os.path.join(folder_path,(kwargs['device']+'.ino')), 'a') as new_file:
                new_file.write(line)
        return f"sketchbook/{kwargs['device']}"

    def delete_file(self, file_name):
        if os.path.exists(file_name+'.ino'):
            os.remove(file_name+'.ino')

if __name__ == '__main__':
    node_file = NodeMCUFile()

    print("Please choice your device name-")
    print("0: Actuator")
    print("1: Soil Moisture Sensor")
    print("2: Ultrasonic Sensor HC-SR04")
    print("3: IR Sensor")
    print("4: Analog Sensor")
    print("5: Digital Sensor")

    d = input("Enter your choice: ")
    owner = input("Sensor Owner Name:")
    device = devices[d]
    ssid = input("Enter your Wifi Network Name: ")
    password  = input("Enter Wifi Password: ")
    longitude = input("Enter Device Longitude: ")
    latitude = input("Enter Device Latitude: ")
    # api = input("Enter your system api: ")

    node_file.delete_file(device)
    node_file.create('file/', owner=owner, ssid=ssid, password=password,
            device=device, longitude=longitude, latitude=latitude) #,api= api)
