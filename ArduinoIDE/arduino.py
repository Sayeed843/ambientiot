"""
Thesis title : Autonomous service creation framework for pluggable IoT Sensors and Actuators
Contributor : Abu Sayeed Bin Mozahid (151-35-843) & MD. Ariful Islam (151-35-937)
Supervisor : K. M. Imtiaz-Ud-Din (Assistant Professor)
Department of Software Engineering
Daffodil International University
Summer 2019
"""

import os
import shutil
import re
from ArduinoIDE.Console.terminal import Terminal
# from terminal import


"""
"virtual-env":"sudo apt install python3-venv -y"
"update": "sudo apt update",
"upgrade": "sudo apt upgrade -y",
"brltty": "sudo apt remove brltty",
"port-permission": "sudo chmod 777 /dev/ttyUSB*"
"""

commands = {
    "arduino-cli": "curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh",
    "move": "sudo mv bin/arduino-cli /usr/bin",
    "init": "arduino-cli config init",
    "update": "arduino-cli core update-index",
    "install": "arduino-cli core install esp8266:esp8266@2.7.4",
    "json": "arduino-cli lib install ArduinoJson@5.13.5",
    "serial-port": "arduino-cli board list",
    "port-permission": "sudo chmod 777 {}",
    "board-check": "arduino-cli board listall | grep -i 'nodemcu'",
    "compile": "arduino-cli compile --fqbn esp8266:esp8266:nodemcuv2 {f}",
    "upload": "arduino-cli upload --port {p} --fqbn esp8266:esp8266:nodemcuv2 {f}",
}

class ArduinoInstallation():
    def __init__(self):
        self.term = Terminal()

    # Command- curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
    def installation(self, cmd):
        self.term.task(cmd)

    # Command- sudo mv bin/arduino-cli /usr/bin
    def move(self, cmd):
        self.term.task(cmd)

    # Command- arduino-cli config init
    def initial_arduino_cli(self, cmd):
        self.term.task(cmd)

    # Command- arduino-cli core update-index
    def update_board(self, cmd):
        self.term.task(cmd)

    def arduino_json(self, cmd):
        self.term.task(cmd)

    def port_permission(self, port, cmd):
        cmd = cmd.format(port[:-1]+"*")
        # cmd = cmd+" "+ port[:-1]+"*"
        self.term.task(cmd)


    # Command- arduino-cli board list
    def serial_port(self, cmd):
        output = self.term.task(cmd)
        port_pattern = r"/dev/tty\w*\d{1}"  # /dev/tty is the based
        return re.findall(port_pattern, output)[0]




class NodeMCUInstallation(ArduinoInstallation):

    # Command- arduino-cli board listall | grep -i 'nodemcu'
    def __init__(self, cmd):
        super(NodeMCUInstallation, self).__init__()
        self.term.task(cmd)

    # Command- arduino-cli board listall | grep -i 'nodemcu'
    def check(self, cmd):
        self.term.task(cmd)


    """
    board_manager:
        additional_urls:
            - http://arduino.esp8266.com/stable/package_esp8266com_index.json
    """
    # Command- /home/<username>/.arduino15/arduino-cli.yaml
    def modify_yaml(self):
        data = ['board_manager:','  additional_urls:'
                ,'    - http://arduino.esp8266.com/stable/package_esp8266com_index.json']

        username = self.term.username()
        path = "/home/{}/.arduino15/arduino-cli.yaml".format(username)

        yaml_file = open(path, "r")
        file =  yaml_file.readlines()
        yaml_file.close()

        try:
            if data[2] not in file[-1]:
                yaml_file = open(path, "a")
                for line in data:
                    yaml_file.write(line)
                    yaml_file.write("\n")

                yaml_file.close()
            else:
                print("The yaml file was updated.")
        except:
            print("The yaml file is empty! You should delete it first.")

    # Command- arduino-cli compile --fqbn esp8266:esp8266:nodemcuv2 <filenamename>
    def compile(self, **kwargs):
        print(kwargs['cmd'].format(f=kwargs['folder']))
        self.term.task(kwargs['cmd'].format(f=kwargs['folder']))


    # Command- arduino-cli upload --port <port>--fqbn esp8266:esp8266:nodemcuv2 <filenamename>
    def upload(self, **kwargs):
        port=self.serial_port(kwargs['port'])
        print(kwargs['cmd'].format(p=port, f=kwargs['folder']))
        self.term.task(kwargs['cmd'].format(p=port, f=kwargs['folder']))


if __name__ == "__main__":

    folder_path = "sketchbook/ir_sensor"
    nodemcu = NodeMCUInstallation("arduino-cli board listall | grep -i 'nodemcu'")
    sleep(1.0)
    # clear_console()

    # Arduino-cli download
    nodemcu.installation(commands['arduino-cli'])
    sleep(1.0)
    # clear_console()

    # Arduino-cli file move
    nodemcu.move(commands['move'])
    sleep(1.0)
    # clear_console()

    # Initialize the arduino-cli
    nodemcu.initial_arduino_cli(commands['init'])
    sleep(1.0)
    # clear_console()

    # Add json file to yaml file
    nodemcu.modify_yaml()
    sleep(1.0)
    # clear_console()

    # Update Arduino-cli board index
    nodemcu.update_board(commands['update'])
    sleep(1.0)
    # clear_console()

    # Install esp8266 library
    nodemcu.installation(commands['install'])
    sleep(1.0)
    # clear_console()

    # Install ArduinoJson library
    nodemcu.arduino_json(commands['json'])
    sleep(1.0)
    # clear_console()

    # Check the serial port
    port = nodemcu.serial_port(commands['serial-port'])
    sleep(1.0)
    # clear_console()

    # Update Arduino-cli board index
    nodemcu.update_board(commands['update'])
    sleep(1.0)
    # clear_console()

    # Check the nodemcu board
    nodemcu.check(commands['board-check'])
    sleep(1.0)
    # clear_console()

    # Compile the .ino file
    nodemcu.compile(folder=folder_path, cmd=commands['compile'])
    sleep(1.0)
    # clear_console()

    # Upload the .ino file
    nodemcu.upload(folder=folder_path, port=commands['serial-port'], cmd=commands['upload'])
    sleep(1.0)
