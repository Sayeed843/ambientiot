#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

void create_tag();
void wifi_setup();
void json_format();
void setup();
void loop();

const char *owner = "owner_name";
const char *WIFI_SSID = "wifi_name";
const char *WIFI_PASS = "wifi_password";
const char *api = "http://192.168.3.126:8000/api/lowersensors/";
const char *service_api = "http://192.168.3.126:8000/api/serviceregistry/";
const String sensor_name = "device_name"; 
const String longitude = "sensor_long";
const String latitude = "sensor_lati";
String sensor_tag = "";

int sensor_value = A0;

  

HTTPClient http_service; 
HTTPClient http;
   
StaticJsonBuffer<500> json_buff;
JsonObject &json_encoder = json_buff.createObject();
char json_message_buffer[500];

StaticJsonBuffer<500> registry_buff;
JsonObject &registry_encoder = registry_buff.createObject();
char service_registry_buffer[500];


void create_tag()
{   sensor_tag = "";
    sensor_tag += sensor_name;
    sensor_tag +="_";
    sensor_tag += longitude;
    sensor_tag +="_";
    sensor_tag += latitude;
    sensor_tag +="_";
    sensor_tag += owner;
    Serial.print("Sensor Tag:");
    Serial.println(sensor_tag);
}



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sensor_value,INPUT);
  setup_wifi();

  create_tag();
  for(int i=0;i<3;i++)
  {
    service_registry_json();
    Serial.println("Service Registry:");
    http_service.begin(service_api);
    http_service.addHeader("Content-Type", "application/json");

    int httpCode = http_service.POST(service_registry_buffer);   //Send the request

    String payload = http_service.getString();                                        //Get the response payload
    
    Serial.print("HTTP Code: ");
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload
 
    http_service.end();
    delay(2000);
  }

}

void loop() {
  // put your main code here, to run repeatedly:

  json_format();
  
  http.begin(api);

  http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
  int httpCode = http.POST(json_message_buffer);   //Send the request
  String payload = http.getString();                                        //Get the response payload
 
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
  http.end();  //Close connection
  delay(5000);
}


void setup_wifi()
{
  Serial.print("Connecting to wifi.");
  WiFi.begin(WIFI_SSID,WIFI_PASS);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");
}

void service_registry_json()
{
  registry_encoder["name"] = sensor_tag;
  registry_encoder["service_type"]= "sensor";
  registry_encoder.prettyPrintTo(service_registry_buffer,sizeof(service_registry_buffer));
  Serial.println(service_registry_buffer); 
}

void json_format()
{
  json_encoder["name"] = sensor_name;
  json_encoder["topic"]= sensor_tag;
  //json_encoder["longitude"] = longitude;
  //json_encoder["latitude"] = latitude;
  //json_encoder["owner"] = owner;
  json_encoder["value"] = analogRead(sensor_value);
  json_encoder.prettyPrintTo(json_message_buffer,sizeof(json_message_buffer));
  Serial.println(json_message_buffer);
}
