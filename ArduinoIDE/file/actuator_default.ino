#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>


void setup_wifi();

const char *owner = "owner_name";
const char *WIFI_SSID = "wifi_name";//"ygvigb";
const char *WIFI_PASS = "wifi_password";//"123456789";
const char *api = "http://192.168.3.126:8000/actuators/";
const char *service_api = "http://192.168.3.126:8000/api/serviceregistry/";
const String actuator_name = "device_name"; 
const String longitude = "actuator_long";
const String latitude = "actuator_lati";
String actuator_tag = "";
String server="";

int motor_output = 2;
int http_code;

HTTPClient http;
HTTPClient http_service; 

StaticJsonBuffer<500> registry_buff;
JsonObject &registry_encoder = registry_buff.createObject();
char service_registry_buffer[500];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(motor_output,OUTPUT);
  setup_wifi();
  digitalWrite(motor_output,LOW);
  create_tag();
  for(int i=0;i<1;i++)
  {
    service_registry_json();
    Serial.println("Service Registry:");
    http_service.begin(service_api);
    http_service.addHeader("Content-Type", "application/json");

    int httpCode = http_service.POST(service_registry_buffer);   //Send the request

    String payload = http_service.getString();                                        //Get the response payload
    
    Serial.print("HTTP Code: ");
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload
 
    http_service.end();
    delay(2000);
  }
   customize_api();


}

void loop() {
  // put your main code here, to run repeatedly:

  //http.begin(api);
  Serial.print("Server API: ");
  Serial.println(server);
  http.begin(server);
  http_code = http.GET();
  
  String payload = http.getString();
  
  Serial.print("Response Code: ");
  Serial.println(http_code);

  Serial.print("Returned data from server: ");
  Serial.println(payload);

  if (http_code == HTTP_CODE_OK)
  {
    //const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
//    const size_t capacity = JSON_OBJECT_SIZE(4) + 70;/
    const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(4) + 70;

    DynamicJsonBuffer jsonBuffer(capacity);
    const char* json = payload.c_str();
    
   // Parse JSON object
    
    JsonArray& root = jsonBuffer.parseArray(json);


    Serial.println(root.success());
    
    if (!root.success()) {
      Serial.println(F("Parsing failed!"));
      return;
    }

    JsonObject& root_0 = root[0];
    const char* root_0_topic = root_0["topic"]; // "mc101"
    const char* root_0_value = root_0["value"]; // "0"
    const char* root_0_time = root_0["time"]; // "2019-01-24T14:58:02.098448Z"
    const char* root_0_name = root_0["name"]; 
    Serial.print("Root: ");
//    Serial.println(F("Response"));
//    const char* actuator_tag = root["topic"];
//    const char* value = root["value"];
//    Serial.println(actuator_tag);
    Serial.print("VAlue: ");
    Serial.println(root_0_value);

    if(root_0_value[0] == '1')
    {
      digitalWrite(motor_output,LOW);
      Serial.println("Show Me LOW");
    }else if(root_0_value[0] == '0')
    {
      digitalWrite(motor_output,HIGH);
      Serial.println("Show Me HIGH");
    }
  }
  else
  {
    Serial.println("Error in response");
  }
  http.end();
  
}

void customize_api()
{
  //create_tag();
  server += api;
  server += "?";
  server += "q=";
  server += actuator_tag;
//  server+="Motor_actuator_longi_actuator_lati_Sayeed";/
}


void setup_wifi()
{
  Serial.print("Connecting to wifi.");
  WiFi.begin(WIFI_SSID,WIFI_PASS);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");
}

void service_registry_json()
{
  registry_encoder["name"] = actuator_tag;
  registry_encoder["service_type"]= "actuator";
  registry_encoder.prettyPrintTo(service_registry_buffer,sizeof(service_registry_buffer));
  Serial.println(service_registry_buffer); 
}

void create_tag()
{
    actuator_tag += actuator_name;
    actuator_tag +="_";
    actuator_tag += longitude;
    actuator_tag +="_";
    actuator_tag += latitude;
    actuator_tag +="_";
    actuator_tag += owner;
    Serial.print("Actuator Tag:");
    Serial.println(actuator_tag);
}
