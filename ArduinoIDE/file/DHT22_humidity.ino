#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <DHT_U.h>
#include <DHT.h>
#define DHTPIN 5
#define DHTTYPE DHT22


float hum;

void create_tag();
void wifi_setup();
void json_format();
void setup();
void loop();

const char *owner = "owner_name";
const char *WIFI_SSID = "wifi_name";
const char *WIFI_PASS = "wifi_password";
const char *api = "http://192.168.3.126:8000/api/lowersensors/";
const String sensor_name = "device_name"; 
const String longitude = "sensor_longitude";
const String latitude = "sensor_latitude";
String sensor_tag = "";

HTTPClient http;
DHT dht(DHTPIN, DHTTYPE);

StaticJsonBuffer<500> json_buff;
JsonObject &json_encoder = json_buff.createObject();
char json_message_buffer[500];


void create_tag()
{
    sensor_tag += sensor_name;
    sensor_tag +="::";
    sensor_tag += longitude;
    sensor_tag +="::";
    sensor_tag += latitude;
    sensor_tag +="::";
    sensor_tag += owner;
    Serial.print("Sensor Tag:");
    Serial.println(sensor_tag);
}



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sensor_value,INPUT);
  setup_wifi();

}

void loop() {
  // put your main code here, to run repeatedly:
  create_tag();
  json_format();
  
  http.begin(api);

  http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
  int httpCode = http.POST(json_message_buffer);   //Send the request
  String payload = http.getString();                                        //Get the response payload
 
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
 
  http.end();  //Close connection
}


void setup_wifi()
{
  Serial.print("Connecting to wifi.");
  WiFi.begin(WIFI_SSID,WIFI_PASS);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");
}

void json_format()
{
  json_encoder["sensor_name"] = sensor_name;
  json_encoder["sensor_tag"]= sensor_tag;
  json_encoder["sensor_longitude"] = longitude;
  json_encoder["sensor_latitude"] = latitude;
  //json_encoder["owner"] = owner;
  json_encoder["sensor_value"] = dht.readHumidity();;
  json_encoder.prettyPrintTo(json_message_buffer,sizeof(json_message_buffer));
  Serial.println(json_message_buffer);
}
