import subprocess
import shutil


class Terminal:

    def username(self):
        return self.task("id -un").strip()

    def task_processing(self, cmd):
        cmd = subprocess.Popen(cmd,shell = True,stdout = subprocess.PIPE,
                               stderr = subprocess.PIPE,stdin = subprocess.PIPE)
        output = str(cmd.stdout.read()+cmd.stderr.read(),"utf-8")
        print(output)
        return output

    def task(self, cmd):
        return self.task_processing(cmd)

if __name__ == "__main__":
    terminal = Terminal()
    terminal.task('sudo apt update')
    terminal.task('sudo apt upgrade -y')
    terminal.task('sudo apt remove brltty -y')
